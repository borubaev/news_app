class ApiConst {
  static const apikey = '233d44cd898e4b34a404dd1d299469b7';
  static String topnews([String? domain]) =>
      'https://newsapi.org/v2/top-headlines?country=${domain ?? 'us'}&apiKey=$apikey';

  static discovernews(content) {
    return 'https://newsapi.org/v2/everything?q=$content&apiKey=$apikey';
  }

  static const imgUrl =
      'https://www.nationalbaptist.com/assets/uploads/2018/03/news.jpg';
}
