import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:news_app/models/articles.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

import '../constants/api_const.dart';
import '../exm_widgets.dart/customTag.dart';
import '../exm_widgets.dart/news0fDay.dart';

class NewsPage extends StatefulWidget {
  NewsPage({Key? key, required this.article}) : super(key: key);
  final Article article;

  @override
  State<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  String data() {
    final int hours = DateTime.now()
            .difference(DateTime.parse(widget.article.publishedAt))
            .inHours %
        24;
    final int days = DateTime.now()
        .difference(DateTime.parse(widget.article.publishedAt))
        .inDays;
    final String data =
        '${days > 1 ? days.toString() + ' days' : days.toString() + ' day'} ${hours > 0 ? hours.toString() + ' h' : ''}';
    return data;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.white),
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back),
        ),
        actions: [
          IconButton(
              onPressed: () {
                Share.share(widget.article.url);
              },
              icon: Icon(
                Icons.share,
              ))
        ],
      ),
      extendBodyBehindAppBar: true,
      body: ListView(
        padding: EdgeInsets.zero,
        children: [
          News0fTheDay(
            padding: const EdgeInsets.all(20),
            height: MediaQuery.of(context).size.height * 0.50,
            width: double.infinity,
            imgUrl: '${widget.article.urlToImage ?? ApiConst.imgUrl}',
            borRadius: 20,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomTag(
                  bgColor: Colors.grey.withAlpha(180),
                  children: [
                    Expanded(
                      child: Text(
                        '${widget.article.author}',
                        style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                            color: Colors.white,
                            overflow: TextOverflow.ellipsis),
                      ),
                    ),
                  ],
                ),
                Text(
                  '${widget.article.title}',
                  style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        height: 1.25,
                      ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
                color: Colors.white),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Wrap(
                  runSpacing: 7.0,
                  spacing: 3.0,
                  alignment: WrapAlignment.start,
                  direction: Axis.horizontal,
                  runAlignment: WrapAlignment.start,
                  crossAxisAlignment: WrapCrossAlignment.start,
                  children: [
                    CustomTag(
                      bgColor: Colors.black.withAlpha(200),
                      children: [
                        Expanded(
                          child: Text(
                            '${widget.article.author} Wright',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(
                                    color: Colors.white,
                                    overflow: TextOverflow.ellipsis),
                          ),
                        ),
                        CustomTag(
                          bgColor: Colors.grey.shade300,
                          children: [
                            const Icon(
                              Icons.timer,
                              size: 18,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(
                              data(),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 18,
                ),
                Text(
                  widget.article.title,
                  style: Theme.of(context)
                      .textTheme
                      .headlineSmall!
                      .copyWith(fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 13,
                ),
                Divider(
                  color: Colors.grey.shade700,
                ),
                const SizedBox(
                  height: 13,
                ),
                Text(
                  '${widget.article.description}',
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                const SizedBox(
                  height: 18,
                ),
                MaterialButton(
                  color: Colors.black12,
                  onPressed: () async {
                    final url = Uri.parse(widget.article.url);
                    if (!await launchUrl(url)) {
                      throw Exception('Could not launch $url');
                    }
                  },
                  child: Text('View in website'),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
