import 'package:flutter/material.dart';
import 'package:news_app/service/fetch_service.dart';

import '../constants/api_const.dart';
import '../exm_widgets.dart/bottomNavBar.dart';
import '../exm_widgets.dart/categoryNews.dart';
import '../exm_widgets.dart/discoverNews.dart';
import '../models/news_model.dart';

class Discover extends StatefulWidget {
  const Discover({super.key});

  @override
  State<Discover> createState() => _DiscoverState();
}

class _DiscoverState extends State<Discover> {
  List<String> tabs = [
    'Health',
    'Politics',
    'Art',
    'Food',
    'Sport',
    'IT',
    'Science'
  ];
  String content = 'health';

  @override
  void initState() {
    super.initState();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: tabs.length,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: false,
        ),
        bottomNavigationBar: const BottomNavBar(
          index: 1,
        ),
        body: ListView(
          padding: EdgeInsets.all(20.0),
          children: [
            DiscoverNews(content: content),
            CategoryNews(
              tabs: tabs,
              content: content,
            ),
          ],
        ),
      ),
    );
  }
}
