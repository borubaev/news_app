import 'package:flutter/material.dart';
import 'package:news_app/models/articles.dart';
import 'package:news_app/models/countries_domains.dart';
import 'package:news_app/models/news_model.dart';
import 'package:news_app/service/fetch_service.dart';
import '../constants/api_const.dart';
import '../exm_widgets.dart/bottomNavBar.dart';
import '../exm_widgets.dart/breakNews.dart';
import '../exm_widgets.dart/customTag.dart';
import '../exm_widgets.dart/news0fDay.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // final List articles = Article.articles;
  TopNews? topNews;
  Future<void> fetchNews([String? domain]) async {
    topNews = null;
    setState(() {});
    topNews = await TopNewsReport().fetchTopNews(domain);
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    fetchNews();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: Container(
          margin: EdgeInsets.all(7),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(28),
              color: Colors.grey.shade200),
          child: PopupMenuButton<Country>(
              icon: Icon(
                Icons.menu,
                color: Colors.black,
              ),
              onSelected: (Country item) async {
                await fetchNews(item.domain);
              },
              itemBuilder: (BuildContext context) {
                return countrySet
                    .map((e) =>
                        PopupMenuItem<Country>(value: e, child: Text(e.name)))
                    .toList();
              }),
        ),
      ),
      extendBodyBehindAppBar: true,
      body: topNews == null
          ? Center(child: CircularProgressIndicator())
          : ListView(
              padding: EdgeInsets.zero,
              children: [
                News0fTheDay(
                  padding: const EdgeInsets.all(20),
                  height: MediaQuery.of(context).size.height * 0.45,
                  width: double.infinity,
                  imgUrl:
                      '${topNews!.article[0].urlToImage ?? ApiConst.imgUrl}',
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomTag(
                        bgColor: Colors.grey.withAlpha(150),
                        children: [
                          Text(
                            'News of the day',
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(color: Colors.white),
                          ),
                        ],
                      ),
                      Text(
                        '${topNews!.article[0].title}',
                        maxLines: 4,
                        style:
                            Theme.of(context).textTheme.headlineSmall!.copyWith(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  height: 1.25,
                                ),
                      ),
                      TextButton(
                          style: TextButton.styleFrom(padding: EdgeInsets.zero),
                          onPressed: () {},
                          child: Row(
                            children: [
                              Text(
                                'Learn more',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .copyWith(color: Colors.white),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              const Icon(
                                Icons.arrow_right_alt,
                                color: Colors.white,
                              )
                            ],
                          ))
                    ],
                  ),
                ),
                BreakingNews(articles: topNews)
              ],
            ),
      bottomNavigationBar: const BottomNavBar(
        index: 0,
      ),
    );
  }
}
