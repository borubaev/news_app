import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:news_app/constants/api_const.dart';
import 'package:news_app/models/news_model.dart';

class TopNewsReport {
  final http.Client client = http.Client();
  Future<TopNews?> fetchTopNews([String? domain]) async {
    final Uri uri = Uri.parse(ApiConst.topnews(domain));
    final http.Response response = await client.get(uri);
    if (response.statusCode == 200 || response.statusCode == 201) {
      final data = jsonDecode(response.body);
      final topNews = TopNews.json(data);
      return topNews;
    }
    return null;
  }
}

class DiscoverReport {
  final http.Client client = http.Client();
  Future<TopNews?> fetchDiscoverNews(content) async {
    final Uri uri = Uri.parse(ApiConst.discovernews(content));
    final http.Response response = await client.get(uri);
    if (response.statusCode == 200 || response.statusCode == 201) {
      final data = jsonDecode(response.body);
      final discoverNews = TopNews.json(data);
      return discoverNews;
    }
    return null;
  }
}
