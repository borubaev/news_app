import 'package:flutter/material.dart';
import 'package:news_app/exm_widgets.dart/discoverNews.dart';
import 'package:news_app/models/news_model.dart';

import '../components/Discover.dart';
import '../components/NewsPage.dart';
import '../constants/api_const.dart';
import '../models/articles.dart';
import 'news0fDay.dart';

class BreakingNews extends StatelessWidget {
  const BreakingNews({Key? key, this.articles}) : super(key: key);

  final TopNews? articles;
  @override
  Widget build(BuildContext context) {
    print(articles!.article[2].urlToImage);
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Breaking news',
                style: Theme.of(context)
                    .textTheme
                    .headlineSmall!
                    .copyWith(fontWeight: FontWeight.bold),
              ),
              TextButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Discover(),
                    ),
                  );
                },
                child: Text(
                  'More',
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
              )
            ],
          ),
          const SizedBox(
            height: 17,
          ),
          SizedBox(
            height: 250,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: articles!.article.length,
              itemBuilder: (context, index) {
                return Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  margin: const EdgeInsets.only(right: 14),
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  NewsPage(article: articles!.article[index])));
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        News0fTheDay(
                          width: MediaQuery.of(context).size.width * 0.5,
                          imgUrl:
                              "${articles!.article[index].urlToImage ?? ApiConst.imgUrl}",
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          articles!.article[index].title,
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge!
                              .copyWith(height: 1),
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          '${DateTime.now().difference(DateTime.parse(articles!.article[index].publishedAt)).inHours} hours ago',
                          style: Theme.of(context).textTheme.bodySmall,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          '${articles!.article[index].author}',
                          style: Theme.of(context).textTheme.bodySmall,
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
