import 'package:flutter/material.dart';
import 'package:news_app/components/NewsPage.dart';

import '../constants/api_const.dart';
import '../models/news_model.dart';
import '../service/fetch_service.dart';

class CategoryNews extends StatefulWidget {
  CategoryNews({
    Key? key,
    required this.tabs,
    required this.content,
  }) : super(key: key);

  final List<String> tabs;
  String content;

  @override
  State<CategoryNews> createState() => _CategoryNewsState();
}

class _CategoryNewsState extends State<CategoryNews>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  TopNews? discoverNews;
  Future<void> fetchDiscoverNews() async {
    discoverNews = await DiscoverReport().fetchDiscoverNews(widget.content);
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    fetchDiscoverNews();
    _tabController = TabController(length: widget.tabs.length, vsync: this);
    _tabController.addListener(tabChange);
    setState(() {});
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  void tabChange() {
    setState(() {
      switch (_tabController.index) {
        case 0:
          widget.content = widget.tabs[_tabController.index];
          break;
      }
      widget.content = widget.tabs[_tabController.index];
      discoverNews = null;
      fetchDiscoverNews();
      // setState(() {});
    });
  }

  String data(index) {
    final int hours = DateTime.now()
            .difference(
                DateTime.parse(discoverNews!.article[index].publishedAt))
            .inHours %
        24;
    final int days = DateTime.now()
        .difference(DateTime.parse(discoverNews!.article[index].publishedAt))
        .inDays;
    final String data =
        '${days > 1 ? days.toString() + ' days' : days.toString() + ' day'} ${hours > 0 ? hours.toString() + ' hours ago' : ''}';
    return data;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TabBar(
          controller: _tabController,
          isScrollable: true,
          indicatorColor: Colors.black,
          tabs: widget.tabs
              .map(
                (tab) => Tab(
                  icon: Text(
                    tab,
                    style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
              )
              .toList(),
          onTap: (index) {
            widget.content = widget.tabs[index];
            discoverNews = null;
            fetchDiscoverNews();
            setState(() {});
          },
        ),
        SizedBox(
          height:
              discoverNews == null ? 300 : MediaQuery.of(context).size.height,
          child: discoverNews == null
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    CircularProgressIndicator(),
                  ],
                )
              : TabBarView(
                  controller: _tabController,
                  children: widget.tabs
                      .map((tab) => ListView.builder(
                            itemCount: discoverNews?.article.length,
                            itemBuilder: ((context, index) {
                              return InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => NewsPage(
                                              article: discoverNews!
                                                  .article[index])));
                                },
                                child: Row(
                                  children: [
                                    Container(
                                      width: 90,
                                      height: 90,
                                      margin: EdgeInsets.all(13),
                                      child: Image.network(
                                        discoverNews!
                                                .article[index].urlToImage ??
                                            ApiConst.imgUrl,
                                        width: 80,
                                        height: 80,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    Expanded(
                                      child: Column(children: [
                                        Text(
                                          '${discoverNews?.article[index].title}',
                                          maxLines: 3,
                                          overflow: TextOverflow.clip,
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyLarge!
                                              .copyWith(
                                                  fontWeight: FontWeight.bold),
                                        ),
                                        const SizedBox(
                                          height: 12,
                                        ),
                                        Row(
                                          children: [
                                            Text(data(index)),
                                          ],
                                        )
                                      ]),
                                    ),
                                  ],
                                ),
                              );
                            }),
                          ))
                      .toList()),
        ),
      ],
    );
  }
}
