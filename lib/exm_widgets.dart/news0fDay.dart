import 'package:flutter/material.dart';
import 'customTag.dart';

class News0fTheDay extends StatelessWidget {
  const News0fTheDay({
    Key? key,
    this.height = 125,
    required this.width,
    required this.imgUrl,
    this.padding,
    this.margin,
    this.borRadius = 20,
    this.child,
  }) : super(key: key);

  final double height;
  final double width;
  final String imgUrl;
  final EdgeInsets? padding;
  final EdgeInsets? margin;
  final double borRadius;
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: padding,
      height: height,
      width: width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(borRadius),
        image: DecorationImage(image: NetworkImage(imgUrl), fit: BoxFit.cover),
      ),
      child: child,
    );
  }
}
