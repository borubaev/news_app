import 'source.dart';

class Article {
  Article({
    this.source,
    required this.author,
    required this.title,
    this.description,
    required this.url,
    this.urlToImage,
    required this.publishedAt,
    this.content,
  });
  final Source? source;
  final String? author;
  final String title;
  final String? description;
  final String url;
  final String? urlToImage;
  final String publishedAt;
  final String? content;

  factory Article.fromJson(Map<String, dynamic> json) {
    return Article(
      source: Source.fromJson(json['source']),
      author: json['author'],
      title: json['title'],
      description: json['description'],
      url: json['url'],
      urlToImage: json['urlToImage'],
      publishedAt: json['publishedAt'],
      content: json['content'],
    );
  }

  // static List<Article> articles = [
  //   Article(
  //     author: "Lawrence Richard",
  //     title:
  //         "Mass shooting at Maryland home leaves 3 dead, several injured: police - Fox News",
  //     description:
  //         "Annapolis police Chief Ed Jackson said a suspect is in custody after a shooting outside a home in Annapolis, Maryland, left at least three people dead and three others injured.",
  //     url:
  //         "https://www.foxnews.com/us/mass-shooting-maryland-home-leaves-3-dead-several-injured-police",
  //     urlToImage:
  //         "https://static.foxnews.com/foxnews.com/content/uploads/2023/06/AP23163077001828.jpg",
  //     publishedAt: "2023-06-12T03:54:00Z",
  //     content:
  //         "A mass shooting at a private residence in an otherwise quiet neighborhood in Annapolis, Maryland, on Sunday left at least three people dead and three others injured.\r\nAnnapolis police Chief Ed Jackso… [+1772 chars]",
  //   ),
  //   Article(
  //     author: "Lawrence Richard",
  //     title:
  //         "Mass shooting at Maryland home leaves 3 dead, several injured: police - Fox News",
  //     description:
  //         "Annapolis police Chief Ed Jackson said a suspect is in custody after a shooting outside a home in Annapolis, Maryland, left at least three people dead and three others injured.",
  //     url:
  //         "https://www.foxnews.com/us/mass-shooting-maryland-home-leaves-3-dead-several-injured-police",
  //     urlToImage:
  //         "https://cdn.photofunia.com/effects/breaking-news/icons/huge.jpg",
  //     publishedAt: "2023-06-12T03:54:00Z",
  //     content:
  //         "A mass shooting at a private residence in an otherwise quiet neighborhood in Annapolis, Maryland, on Sunday left at least three people dead and three others injured.\r\nAnnapolis police Chief Ed Jackso… [+1772 chars]",
  //   )
  // ];
}

// final List arr = [
//   {
//     "source": {"id": "fox-news", "name": "Fox News"},
//     "author": "Lawrence Richard",
//     "title":
//         ,
//     "description":
//        ,
//     "url":
//         ,
//     "urlToImage":
//         ,
//     "publishedAt": ,
//     "content":
       
//   }
// ];
