class Country {
  const Country({required this.name, required this.domain});

  final String name;
  final String domain;
}

const Country oae = Country(name: 'OAE', domain: 'ae');
const Country poland = Country(name: 'Poland', domain: 'pl');
const Country russia = Country(name: 'Russia', domain: 'ru');
const Country usa = Country(name: 'USA', domain: 'us');
const Country italy = Country(name: 'Italy', domain: 'it');
const Country france = Country(name: 'France', domain: 'fr');
const Country korea = Country(name: 'Korea', domain: 'kr');
const Country turkey = Country(name: 'Turkey', domain: 'tr');
const Country china = Country(name: 'China', domain: 'cn');

Set<Country> countrySet = {
  oae,
  poland,
  russia,
  usa,
  italy,
  france,
  korea,
  turkey,
  china,
};
